package com.example.obviouexercise;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.InputStream;
import java.io.IOException;



public class MainActivity extends AppCompatActivity {

    GridView gridView;
    ArrayList items = new ArrayList<>();

    public String readJSON() {
        String json = null;
        try {
            InputStream inputStream = getAssets().open("data.json");
            int size = inputStream.available();
            byte[] buffer = new byte[size];

            inputStream.read(buffer);
            inputStream.close();

            json = new String(buffer, "UTF-8");
        } catch (IOException e) {
            e.printStackTrace();
            return json;
        }
        return json;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        try {
            JSONArray array = new JSONArray(readJSON());

            for (int i = 0; i < array.length(); i++) {

                JSONObject jsonObject = array.getJSONObject(i);
                String explanation = jsonObject.getString("explanation");
                String image = jsonObject.getString("url");
                String title = jsonObject.getString("title");
                String date = jsonObject.getString("date");

                items.add(new GridItem(explanation,image, title, date));

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


        gridView = (GridView) findViewById(R.id.gridView);
        gridView.setAdapter(new GridViewAdapter(this, items));


    }



}