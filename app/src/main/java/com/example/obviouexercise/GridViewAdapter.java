package com.example.obviouexercise;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import java.util.ArrayList;
import com.squareup.picasso.Picasso;



public class GridViewAdapter extends BaseAdapter  {
    private Context mContext;
    private ArrayList<GridItem> items;

    public GridViewAdapter(Context c,ArrayList<GridItem> items ) {
        mContext = c;

        this.items = items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int p) {
        return null;
    }

    @Override
    public long getItemId(int p) {
        return 0;
    }

    @Override
    public View getView(int p, View convertView, ViewGroup parent) {
        View grid;
        LayoutInflater inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {

            grid = new View(mContext);
            grid = inflater.inflate(R.layout.grid_item_layout, null);
            ImageView imageView = (ImageView)grid.findViewById(R.id.image);


            String url = items.get(p).getImage();
            Picasso.get()
                    .load(url)
                    .placeholder(R.drawable.loading)
                    .fit()
                    .centerCrop().into(imageView);
        } else {
            grid = (View) convertView;
        }


        grid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, FullScreenPage.class);
                intent.putExtra("list", items);
                intent.putExtra("pos", p);

                mContext.startActivity(intent);

            }
        });

        return grid;

    }



}