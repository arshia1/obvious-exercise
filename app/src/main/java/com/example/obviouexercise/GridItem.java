package com.example.obviouexercise;

import java.io.Serializable;

public class GridItem implements Serializable {

    String explanation;
    String image;
    String title;
    String date;

    public GridItem(String explanation,String image, String title, String date)
    {
        this.image=image;
        this.explanation=explanation;
        this.title = title;
        this.date = date;
    }
    public String getExplanation()
    {
        return explanation;
    }
    public String  getImage()
    {
        return image;
    }
    public String getTitle()
    {
        return title;
    }
    public String getDate()
    {
        return date;
    }


}
