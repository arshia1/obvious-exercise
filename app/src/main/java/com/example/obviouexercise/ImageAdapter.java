package com.example.obviouexercise;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;

import com.squareup.picasso.Picasso;


import java.util.List;

public class ImageAdapter extends PagerAdapter {
    Context context;
    List<GridItem> items;

    LayoutInflater mLayoutInflater;

    ImageAdapter(Context context, List<GridItem> items){
        this.context=context;
        this.items = items;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.imageView);

        TextView explanationText = (TextView) itemView.findViewById(R.id.explanation);
        TextView titleText = (TextView) itemView.findViewById(R.id.title);
        TextView dateText = (TextView) itemView.findViewById(R.id.date);


        explanationText.setText(items.get(position).explanation);
        titleText.setText(items.get(position).title);
        dateText.setText(items.get(position).date);


        Picasso.get()
                .load(items.get(position).getImage())
                .placeholder(R.drawable.loading)
                .fit()
                .centerInside().into(imageView);


        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout)object);
    }
}